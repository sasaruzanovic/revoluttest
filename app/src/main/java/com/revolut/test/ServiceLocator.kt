package com.revolut.test

import android.content.Context
import androidx.room.Room
import com.revolut.test.data.DefaultExchangeRepository
import com.revolut.test.data.ExchangeRepository
import com.revolut.test.data.local.ExchangeDatabase
import com.revolut.test.data.local.ExchangeLocalDataImpl
import com.revolut.test.data.remote.ExchangeDataRemoteImpl

object ServiceLocator {

    private var database: ExchangeDatabase? = null
    private var exchangeRepository: ExchangeRepository? = null

    fun provideExchangeRepository(context: Context): ExchangeRepository {
        synchronized(this) {
            return exchangeRepository ?: createExchangeRepository(context)
        }
    }

    private fun createExchangeRepository(context: Context): ExchangeRepository {
        val newRepo =
            DefaultExchangeRepository(ExchangeDataRemoteImpl, createExchangeLocalDataSource(context))
        exchangeRepository = newRepo
        return newRepo
    }


    private fun createExchangeLocalDataSource(context: Context): ExchangeLocalDataImpl {
        val database = database ?: createDataBase(context)
        return ExchangeLocalDataImpl(database.exchangeDao())
    }

    private fun createDataBase(context: Context): ExchangeDatabase {
        val result = Room.databaseBuilder(
            context.applicationContext,
            ExchangeDatabase::class.java, DB_NAME
        ).build()
        database = result
        return result
    }
}

private const val DB_NAME = "exchange.db"
