package com.revolut.test.exchange

import androidx.lifecycle.*
import com.revolut.test.data.ExchangeRepository
import kotlinx.coroutines.Dispatchers


class ExchangeViewModel(exchangeRepository: ExchangeRepository) : ViewModel() {

    private val refresh = MutableLiveData<Boolean>()
    var refreshedData = Transformations.switchMap(refresh) {
        liveData(Dispatchers.IO) {
            emit(exchangeRepository.getExchange())
        }
    }

    val exchangeLiveData = liveData(Dispatchers.IO) {
        emit(exchangeRepository.getExchange())
    }

    fun refresh() = apply {
        refresh.postValue(true)
    }
}
