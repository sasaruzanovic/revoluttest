package com.revolut.test.exchange

import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.revolut.test.R
import com.revolut.test.data.local.Exchange
import com.revolut.test.utils.SimpleTextWatcher
import kotlinx.android.synthetic.main.exchange_list_item.view.*


class ExchangeAdapter : RecyclerView.Adapter<ExchangeAdapter.ViewHolder>(),
    ExchangeMachine.RefreshListener {

    private val items = ArrayList<Exchange>()
    private val listeners = ArrayList<TextWatcher>()
    private val exchangeMachine = ExchangeMachine(this)

    fun setItems(newItems: List<Exchange>) {
        items.addAll(exchangeMachine.prepareItems(newItems))
        notifyDataSetChanged()
    }

    fun refreshItems(newItems: List<Exchange>) {
        exchangeMachine.refreshMultipliers(newItems)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.exchange_list_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        listeners.forEach {
            (holder.edtValue.removeTextChangedListener(it))
        }

        Glide.with(holder.imgCountry)
            .load(items[position].image)
            .apply(RequestOptions.circleCropTransform())
            .into(holder.imgCountry);
        holder.txtTitle.text = items[position].id
        holder.txtDesc.text = items[position].id
        holder.edtValue.setText(items[position].amount.toString())
        holder.edtValue.setSelection(items[position].amount.toString().length)

        val watcher = SimpleTextWatcher(object : SimpleTextWatcher.OnTextChanged {
            override fun afterTextChanged(text: String) {
                exchangeMachine.refreshAmount(holder.adapterPosition, text)
            }
        })

        listeners.add(watcher as TextWatcher)
        holder.edtValue.addTextChangedListener(watcher)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txtTitle: TextView = view.txt_currency_title
        val txtDesc: TextView = view.txt_currency_desc
        val imgCountry: ImageView = view.img_country
        val edtValue: EditText = view.edt_currency_value

    }

    override fun onItemRefreshed(position: Int) {
        notifyItemChanged(position)
    }

}