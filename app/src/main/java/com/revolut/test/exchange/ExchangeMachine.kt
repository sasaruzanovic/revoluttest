package com.revolut.test.exchange

import com.revolut.test.data.local.Exchange
import java.math.BigDecimal
import java.math.RoundingMode

class ExchangeMachine(private val refreshListener: RefreshListener) {

    var baseAmount: BigDecimal = BigDecimal(1.000).setScale(3, RoundingMode.HALF_UP)
    val items = ArrayList<Exchange>()
    var currentPosition = 0;
    var currentAmount = BigDecimal(0)

    fun prepareItems(newItems: List<Exchange>): List<Exchange> {
        newItems.forEach { exchange -> exchange.amount = exchange.multiplier.times(baseAmount) }
        items.addAll(newItems)
        return items;
    }

    fun refreshAmount(positionToSkip: Int, amount: String) {

        currentPosition = positionToSkip

        if (amount.isNotEmpty()) {
            currentAmount = amount.toBigDecimal()
        }
        val currentExchange = items[positionToSkip]

        baseAmount = currentAmount.divide(currentExchange.multiplier, 3, RoundingMode.CEILING)

        for (i in 0 until items.size) {
            if (positionToSkip != i) {
                items[i].amount = items[i].multiplier.multiply(baseAmount)
                    .setScale(3, RoundingMode.HALF_UP)
                refreshListener.onItemRefreshed(i)
            }
        }
    }


    fun refreshMultipliers(newItems: List<Exchange>) {

        for (i in 0 until items.size) {
            if (items[i].multiplier != newItems[i].multiplier) {
                items[i].multiplier = newItems[i].multiplier
                if (currentPosition != i) {
                    items[i].amount = items[i].multiplier.multiply(baseAmount)
                        .setScale(3, RoundingMode.HALF_UP)
                    refreshListener.onItemRefreshed(i)
                } else {
                    baseAmount =
                        currentAmount.divide(newItems[i].multiplier, 3, RoundingMode.CEILING)
                }
            }
        }

        items[0].amount = baseAmount
        if (currentPosition != 0) {
            refreshListener.onItemRefreshed(0)
        }
    }

    interface RefreshListener {
        fun onItemRefreshed(position: Int)
    }
}

