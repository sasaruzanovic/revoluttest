package com.revolut.test.data.local

import com.revolut.test.data.Result


/**
Main entry point for accessing exchange data.
 */
interface ExchangeDataLocalSource {

    suspend fun getExchange(): Result<List<Exchange>>
    suspend fun saveExchange(exchange: Exchange)

}