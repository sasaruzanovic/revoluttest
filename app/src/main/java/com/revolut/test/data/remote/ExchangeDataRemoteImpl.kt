package com.revolut.test.data.remote

import com.revolut.test.BuildConfig
import com.revolut.test.data.Result
import com.revolut.test.data.local.Exchange
import com.revolut.test.data.remote.services.RevolutService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

object ExchangeDataRemoteImpl : ExchangeDataRemoteSource {

    private suspend fun getData(currency: String): ApiResponse<RevolutData> {
        return safeApiCall(
            call = { createService(RevolutService::class.java).getData(currency) })
    }


    private fun <S> createService(serviceClass: Class<S>): S {
        val httpClient = OkHttpClient.Builder()

        setTimeout(httpClient)

        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(interceptor)
        }

        val builder = Retrofit.Builder()
            .baseUrl("https://hiring.revolut.codes/")
            .addConverterFactory(GsonConverterFactory.create())

        val client = httpClient.build()

        val retrofit = builder.client(client).build()
        return retrofit.create(serviceClass)

    }

    private fun setTimeout(httpClient: OkHttpClient.Builder) {
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
    }

    private suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): ApiResponse<T> {
        return try {
            ApiResponse.create(call())
        } catch (e: Exception) {
            // An exception was thrown when calling the API so we're converting this to an IOException
            ApiErrorResponse("error")
        }
    }


    override suspend fun getExchange(): Result<List<Exchange>> {

        when (val data = getData("EUR")) {
            is ApiSuccessResponse -> {
                val revolutData = data.data
                val exchangeList = ArrayList<Exchange>()

                exchangeList.add(Exchange("EUR"))
                exchangeList.add(Exchange("AUD", BigDecimal(revolutData.rates.AUD)))
                exchangeList.add(Exchange("BGN", BigDecimal(revolutData.rates.BGN)))
                exchangeList.add(Exchange("BRL", BigDecimal(revolutData.rates.BRL)))
                exchangeList.add(Exchange("CAD", BigDecimal(revolutData.rates.CAD)))
                exchangeList.add(Exchange("CHF", BigDecimal(revolutData.rates.CHF)))
                exchangeList.add(Exchange("CNY", BigDecimal(revolutData.rates.CNY)))
                exchangeList.add(Exchange("CZK", BigDecimal(revolutData.rates.CZK)))
                exchangeList.add(Exchange("DKK", BigDecimal(revolutData.rates.DKK)))
                exchangeList.add(Exchange("GBP", BigDecimal(revolutData.rates.GBP)))
                exchangeList.add(Exchange("HKD", BigDecimal(revolutData.rates.HKD)))
                exchangeList.add(Exchange("HRK", BigDecimal(revolutData.rates.HRK)))
                exchangeList.add(Exchange("HUF", BigDecimal(revolutData.rates.HUF)))
                exchangeList.add(Exchange("IDR", BigDecimal(revolutData.rates.IDR)))
                exchangeList.add(Exchange("ILS", BigDecimal(revolutData.rates.ILS)))
                exchangeList.add(Exchange("INR", BigDecimal(revolutData.rates.INR)))
                exchangeList.add(Exchange("ISK", BigDecimal(revolutData.rates.ISK)))
                exchangeList.add(Exchange("JPY", BigDecimal(revolutData.rates.JPY)))
                exchangeList.add(Exchange("KRW", BigDecimal(revolutData.rates.KRW)))
                exchangeList.add(Exchange("MXN", BigDecimal(revolutData.rates.MXN)))
                exchangeList.add(Exchange("MYR", BigDecimal(revolutData.rates.MYR)))
                exchangeList.add(Exchange("NOK", BigDecimal(revolutData.rates.NOK)))
                exchangeList.add(Exchange("NZD", BigDecimal(revolutData.rates.NZD)))
                exchangeList.add(Exchange("PHP", BigDecimal(revolutData.rates.PHP)))
                exchangeList.add(Exchange("PLN", BigDecimal(revolutData.rates.PLN)))
                exchangeList.add(Exchange("RON", BigDecimal(revolutData.rates.RON)))
                exchangeList.add(Exchange("RUB", BigDecimal(revolutData.rates.RUB)))
                exchangeList.add(Exchange("SEK", BigDecimal(revolutData.rates.SEK)))
                exchangeList.add(Exchange("SGD", BigDecimal(revolutData.rates.SGD)))
                exchangeList.add(Exchange("THB", BigDecimal(revolutData.rates.THB)))
                exchangeList.add(Exchange("USD", BigDecimal(revolutData.rates.USD)))

                return Result.Success(exchangeList)
            }
            is ApiSuccessEmptyResponse -> return Result.Error(Exception("Error"))
            is ApiErrorResponse -> return Result.Error(Exception("Error"))
        }
    }


}