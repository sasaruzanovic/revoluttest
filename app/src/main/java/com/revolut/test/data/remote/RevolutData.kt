package com.revolut.test.data.remote

data class RevolutData(
    val baseCurrency: String,
    val rates: Rates
)

data class Rates(
    val AUD: String,
    val BGN: String,
    val BRL: String,
    val CAD: String,
    val CHF: String,
    val CNY: String,
    val CZK: String,
    val DKK: String,
    val GBP: String,
    val HKD: String,
    val HRK: String,
    val HUF: String,
    val IDR: String,
    val ILS: String,
    val INR: String,
    val ISK: String,
    val JPY: String,
    val KRW: String,
    val MXN: String,
    val MYR: String,
    val NOK: String,
    val NZD: String,
    val PHP: String,
    val PLN: String,
    val RON: String,
    val RUB: String,
    val SEK: String,
    val SGD: String,
    val THB: String,
    val USD: String)