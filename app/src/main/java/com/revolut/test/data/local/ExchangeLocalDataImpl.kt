package com.revolut.test.data.local

import com.revolut.test.data.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ExchangeLocalDataImpl internal constructor(
    private val exchangeDao: ExchangeDao,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ExchangeDataLocalSource {

    override suspend fun getExchange(): Result<List<Exchange>> = withContext(ioDispatcher) {
        return@withContext try {
            Result.Success(exchangeDao.getExchange())
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun saveExchange(exchange: Exchange) {
        exchangeDao.insertExchange(exchange)
    }
}