package com.revolut.test.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
@Dao
interface ExchangeDao {
    @Query("SELECT * FROM exchange")
    fun getExchange(): List<Exchange>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertExchange(exchange: Exchange)
}