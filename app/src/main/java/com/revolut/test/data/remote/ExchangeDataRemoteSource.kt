package com.revolut.test.data.remote

import com.revolut.test.data.Result
import com.revolut.test.data.local.Exchange

/**
Main entry point for accessing exchange data.
 */
interface ExchangeDataRemoteSource {

    suspend fun getExchange(): Result<List<Exchange>>

}