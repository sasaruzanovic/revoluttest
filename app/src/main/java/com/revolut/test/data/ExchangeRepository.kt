package com.revolut.test.data

import com.revolut.test.data.local.Exchange

interface ExchangeRepository {

    suspend fun getExchange(forceUpdate: Boolean = true): Result<List<Exchange>>
}