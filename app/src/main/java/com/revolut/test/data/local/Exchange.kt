package com.revolut.test.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.math.BigDecimal

@Entity(tableName = "exchange")
data class Exchange constructor(
    @PrimaryKey @ColumnInfo(name = "id") var id: String,
    @ColumnInfo(name = "multiplier") var multiplier: BigDecimal = BigDecimal(1.0),
    @ColumnInfo(name = "image") var image: String = "https://image.shutterstock.com/image-vector/flag-european-union-correct-proportions-600w-128803501.jpg",
    var amount: BigDecimal = BigDecimal(1.0)
)