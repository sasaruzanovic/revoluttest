
package com.revolut.test.data.remote.services


import com.revolut.test.data.remote.RevolutData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface RevolutService {

    @GET("/api/android/latest")
    suspend fun getData(@Query("base") base: String): Response<RevolutData>

}
