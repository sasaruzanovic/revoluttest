package com.revolut.test.data

import com.revolut.test.data.local.Exchange
import com.revolut.test.data.local.ExchangeDataLocalSource
import com.revolut.test.data.remote.ExchangeDataRemoteSource
import java.lang.Exception

class DefaultExchangeRepository(
    private val remoteExchangeDataRemoteSource: ExchangeDataRemoteSource,
    private val localExchangeDataRemoteSource: ExchangeDataLocalSource
) : ExchangeRepository {
    override suspend fun getExchange(forceUpdate: Boolean): Result<List<Exchange>> {

        if (forceUpdate) {
            try {
                updateExchangeFromRemoteDataSource()
            } catch (ex: Exception) {
                return Result.Error(Exception(ex))
            }
        }
        return localExchangeDataRemoteSource.getExchange()
    }


    private suspend fun updateExchangeFromRemoteDataSource() {
        val remoteExchange = remoteExchangeDataRemoteSource.getExchange()

        if (remoteExchange is Result.Success) {
            remoteExchange.data.forEach { exchange ->
                localExchangeDataRemoteSource.saveExchange(exchange)
            }
        } else if (remoteExchange is Result.Error) {
            throw remoteExchange.exception
        }
    }
}