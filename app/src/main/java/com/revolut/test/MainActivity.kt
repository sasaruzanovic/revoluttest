package com.revolut.test

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.ItemAnimator
import androidx.recyclerview.widget.SimpleItemAnimator
import com.revolut.test.data.Result
import com.revolut.test.exchange.ExchangeAdapter
import com.revolut.test.exchange.ExchangeViewModel
import com.revolut.test.utils.ViewModelFactory
import kotlinx.android.synthetic.main.main_activity.*
import java.util.*
import kotlin.concurrent.timer


class MainActivity : AppCompatActivity() {

    private val model: ExchangeViewModel by viewModels() { getViewModelFactory() }
    private val adapter = ExchangeAdapter()
    private var timer: Timer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        rv_exchange_list.layoutManager = LinearLayoutManager(this)
        rv_exchange_list.adapter = adapter

        //Glide flickering fix
        val animator: ItemAnimator? = rv_exchange_list.itemAnimator
        if (animator is SimpleItemAnimator) {
            animator.supportsChangeAnimations = false
        }

        model.exchangeLiveData.observe(this, Observer { exchange ->

            when (exchange) {
                is Result.Success -> {
                    adapter.setItems(exchange.data)
                }

                is Result.Error -> Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
            }
        })

        model.refreshedData.observe(this, Observer { exchange ->

            when (exchange) {
                is Result.Success -> {
                    adapter.refreshItems(exchange.data)
                }

                is Result.Error -> Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
            }
        })

        timer = timer(period = 1000, initialDelay = 1000) {
            model.refresh()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        timer?.let {
            it.purge()
            it.cancel()
        }
    }

    private fun getViewModelFactory(): ViewModelFactory {
        val repository = (applicationContext as RevolutApp).taskRepository
        return ViewModelFactory(repository, this)
    }

}
