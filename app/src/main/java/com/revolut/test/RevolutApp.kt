package com.revolut.test

import android.app.Application
import com.revolut.test.data.ExchangeRepository

class RevolutApp : Application() {
    val taskRepository: ExchangeRepository
        get() = ServiceLocator.provideExchangeRepository(this)
}