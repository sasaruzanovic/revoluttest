package com.revolut.test.utils

import android.text.Editable
import android.text.TextWatcher

class SimpleTextWatcher(private val onTextChanged: OnTextChanged) : TextWatcher {
    override fun afterTextChanged(s: Editable?) {
        onTextChanged.afterTextChanged(s.toString())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }


    interface OnTextChanged {
        fun afterTextChanged(text: String)
    }
}