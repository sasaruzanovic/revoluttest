package com.revolut.test.utils

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.revolut.test.data.ExchangeRepository
import com.revolut.test.exchange.ExchangeViewModel

@Suppress("UNCHECKED_CAST")
class ViewModelFactory constructor(
    private val exchangeRepository: ExchangeRepository,
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle? = null
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    override fun <T : ViewModel> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ) = with(modelClass) {
        ExchangeViewModel(exchangeRepository)
    } as T
}