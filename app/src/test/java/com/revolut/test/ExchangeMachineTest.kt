package com.revolut.test

import com.revolut.test.data.local.Exchange
import com.revolut.test.exchange.ExchangeMachine
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode

class ExchangeMachineTest {

    private lateinit var machine: ExchangeMachine
    private val exchangeList = ArrayList<Exchange>()

    @Before
    fun init() {

        exchangeList.addAll(generateList())

        val listener = object : ExchangeMachine.RefreshListener {
            override fun onItemRefreshed(position: Int) {
            }
        }
        machine = ExchangeMachine(listener)
    }

    @Test
    fun prepareItems() {

        machine.prepareItems(exchangeList)
        machine.items.forEach {
            assertThat(it.amount, `is`(it.multiplier.times(machine.baseAmount)))
        }
    }

    @Test
    fun refreshAmount() {

        val oldExchangeList = generateList()
        val amount = BigDecimal("2.22")
        val position = 1
        machine.prepareItems(exchangeList)
        machine.refreshAmount(position, amount.toString())

        assertThat(
            machine.baseAmount,
            `is`(amount.divide(exchangeList[position].multiplier, 3, RoundingMode.CEILING))
        )


        for (i in 0 until exchangeList.size) {
            if (position != i) {
                assertThat(
                    exchangeList[i].amount,
                    `is`(
                        oldExchangeList[i].multiplier.multiply(machine.baseAmount).setScale(
                            3,
                            RoundingMode.HALF_UP
                        )
                    )
                )
            }
        }

    }

    @Test
    fun refreshMultipliers() {
        val oldExchangeList = generateList()
        machine.prepareItems(exchangeList)

        val newExchangeList = generateList(2)
        machine.refreshMultipliers(newExchangeList)

        assertThat(
            machine.baseAmount,
            `is`(
                newExchangeList[machine.currentPosition].amount.divide(
                    exchangeList[machine.currentPosition].multiplier,
                    3,
                    RoundingMode.CEILING
                )
            )
        )


        for (i in 0 until exchangeList.size) {
            if (machine.currentPosition != i) {
                assertThat(
                    exchangeList[i].amount,
                    `is`(
                        oldExchangeList[i].multiplier.multiply(machine.baseAmount).multiply(
                            BigDecimal(2)
                        ).setScale(
                            3,
                            RoundingMode.HALF_UP
                        )
                    )
                )
            }
        }
    }


    private fun generateList(multiplier: Int = 1): List<Exchange> {

        val exchangeList = ArrayList<Exchange>()

        exchangeList.add(Exchange("EUR"))
        exchangeList.add(Exchange("AUD", BigDecimal("1.22").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("BGN", BigDecimal("2.3").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("BRL", BigDecimal("1.59").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("CAD", BigDecimal("3.63").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("CHF", BigDecimal("4.53").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("CNY", BigDecimal("5.5").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("CZK", BigDecimal("4.64").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("DKK", BigDecimal("3.33").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("GBP", BigDecimal("2.53").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("HKD", BigDecimal("6.72").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("HRK", BigDecimal("23.2").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("HUF", BigDecimal("5.4").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("IDR", BigDecimal("5.7").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("ILS", BigDecimal("8.23").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("INR", BigDecimal("23.78").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("ISK", BigDecimal("45.4").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("JPY", BigDecimal("2.334").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("KRW", BigDecimal("2.43").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("MXN", BigDecimal("3.456").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("MYR", BigDecimal("8.6").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("NOK", BigDecimal("4.3").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("NZD", BigDecimal("7.66").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("PHP", BigDecimal("3.23").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("PLN", BigDecimal("5.66").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("RON", BigDecimal("3.44").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("RUB", BigDecimal("3.22").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("SEK", BigDecimal("2.99").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("SGD", BigDecimal("5.55").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("THB", BigDecimal("8.88").multiply(multiplier.toBigDecimal())))
        exchangeList.add(Exchange("USD", BigDecimal("2.77").multiply(multiplier.toBigDecimal())))

        return exchangeList;
    }

}
